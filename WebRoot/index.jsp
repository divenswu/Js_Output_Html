<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>innerHTML</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<script type="text/javascript" src="<%=basePath%>js/innerHTML.js"></script>
	
	<script type="text/javascript">
		function setcontent(){
			var obj_id = 'inner';
			var html ='';
			var time =20;
			
			var sHTML="<input type=button onclick=" + "run()" + " value='Click Me'><BR>";
			var sScript="<SCRIPT>alert('Hello from inserted script.')</SCRIPT" + ">";
			
			html=sHTML+sScript;
			
			set_innerHTML(obj_id,html,time);
		}
		
		function run(){
			alert("you clicked the button");
		}
	</script>
  </head>
  
  <body>
    <input type="button" value="测试innerHTML" onclick="setcontent();"><br/>
    <div id="inner"></div>
  </body>
</html>
